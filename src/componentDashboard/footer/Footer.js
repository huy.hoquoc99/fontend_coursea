import React from "react";
import { Layout } from "antd";
import "antd/dist/antd.css";

const { Footer } = Layout;

const FooterDash = () => {
  return (
    <Footer style={{ textAlign: "center" }}>
      Ant Design ©2018 Created by Ant UED
    </Footer>
  );
};

export default FooterDash;
