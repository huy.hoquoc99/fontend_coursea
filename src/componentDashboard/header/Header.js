import React from "react";
import { Layout } from "antd";
import "antd/dist/antd.css";

const { Header } = Layout;

const HeaderDash = () => {
  return <Header className="site-layout-background" style={{ padding: 0 }} />;
};

export default HeaderDash;
