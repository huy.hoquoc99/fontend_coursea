export default {
  "app.weeks.validate.timeComplete": "Thời gian hoàn thành phải là phút",
  "app.weeks.validate.header": "Tiêu đề tuần phải là chuỗi",
  "app.weeks.validate.description": "Miêu tả tuần phải là chuỗi",
  "app.weeks.validate.create": "Thêm mới tài khoản thành công",
  "app.weeks.validate.edit": "Cập nhật thông tin thành công",
};
