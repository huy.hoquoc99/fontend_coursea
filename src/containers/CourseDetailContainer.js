import { connect } from "react-redux";
import CourseDetail from "../componentWeb/courseDetail/CourseDetail";
import * as courseActions from "../actions/courseActions";

const mapStateToProps = ({ course }) => {
  return {
    course,
  };
};

// const mapStateToProps = (state) => state;

const mapDispatchToProps = (dispatch) => ({
  requestCourseDetail(id) {
    dispatch(courseActions.requestCourseDetail(id));
  },
  requestBuyCourse(param) {
    dispatch(courseActions.requestBuyCourse(param));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetail);
