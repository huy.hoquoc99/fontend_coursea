import { connect } from "react-redux";
// import { fetchPosts, resetDeletedPost, deletePost, deletePostSuccess, deletePostFailure } from '../actions/posts';
// import { logoutUser } from '../actions/users';
import MyCourse from "../componentWeb/myCourse/MyCourse";
import * as userCourseActions from "../actions/userCourseActions";

const mapStateToProps = ({ userCourse }) => {
  // debugger;
  return {
    userCourse,
  };
};

const mapDispatchToProps = (dispatch) => ({
  requestUserCourse(filter, sort, range) {
    dispatch(userCourseActions.requestUserCourse(filter, sort, range));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCourse);
