import React, { Component, Fragment } from "react";
import { Layout } from "antd";
import Sider from "../../../componentDashboard/sider/Sider";
import Footer from "../../../componentDashboard/footer/Footer";
import CourseContainer from "../../../containerDashboard/course/CourseContainer";

class CourseDash extends Component {
  render() {
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider />
        <Layout className="site-layout">
          <CourseContainer />
          <Footer />
        </Layout>
      </Layout>
    );
  }
}

export default CourseDash;
