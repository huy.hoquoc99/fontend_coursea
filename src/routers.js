import React from "react";
import { Route } from "react-router-dom";
// web
import PostIndex from "./pages/web/PostIndex";
import Login from "./pages/web/Login";
import Signin from "./pages/web/Signin";
import HomePage from "./pages/web/HomePage";
import CourseList from "./pages/web/CourseList";
import CourseDetail from "./pages/web/CourseDetail";
import Cart from "./pages/web/Cart";
import PaymentSuccess from "./pages/web/PaymentSuccess";
import MyCourse from "./pages/web/MyCourse";
import Learn from "./pages/web/Learn";
// dashboard
import CourseDash from "./pages/dashboard/course/CourseDash";
import AddCourseDash from "./pages/dashboard/course/AddCourseDash";
import UpdateCourseDash from "./pages/dashboard/course/UpdateCourseDash";
import AddWeekDash from "./pages/dashboard/week/AddWeekDash";
import WeekDash from "./pages/dashboard/week/WeekDash";
import Dashboard from "./pages/dashboard/Dashboard";
import { PrivateRoute } from "./privateRoute";
// router duoc bao ve
// eslint-disable-next-line react/display-name

export default (
  <>
    <Route path="/" component={PostIndex} />
    <Route exact path="/" component={HomePage} />
    <Route exact path="/browser" component={CourseList} />
    <Route exact path="/browser/:topic" component={CourseList} />
    <Route path="/browser/:topic/:category" component={CourseList} />
    <Route path="/learn/:title" component={CourseDetail} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/signin" component={Signin} />
    <Route exact path="/cart" component={Cart} />
    <Route exact path="/myCourse" component={MyCourse} />
    <Route exact path="/paymentSuccess" component={PaymentSuccess} />
    <Route exact path="/myCourse/learn/:title" component={Learn} />

    <Route
      exact
      path="/dashboard"
      /*  render={RouteGuard(Dashboard)} */ component={Dashboard}
    />
    <Route
      exact
      path="/dashboard/category/courses"
      /*  render={RouteGuard(Dashboard)} */ component={CourseDash}
    />

    <Route
      exact
      path="/dashboard/category/courses/create"
      /*  render={RouteGuard(Dashboard)} */ component={AddCourseDash}
    />
    <Route
      exact
      path="/dashboard/category/courses/:id"
      /*  render={RouteGuard(Dashboard)} */ component={UpdateCourseDash}
    />

    <Route
      exact
      path="/dashboard/category/weeks"
      /*  render={RouteGuard(Dashboard)} */ component={WeekDash}
    />

    <Route
      exact
      path="/dashboard/category/weeks/create"
      /*  render={RouteGuard(Dashboard)} */ component={AddWeekDash}
    />
  </>
);

// <PrivateRoute exact path="/dashboard" component={Dashboard} />
//     <PrivateRoute
//       exact
//       path="/dashboard/category/courses"
//       component={CourseDash}
//     />
